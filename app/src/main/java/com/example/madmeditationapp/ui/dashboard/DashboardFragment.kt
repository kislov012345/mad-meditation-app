package com.example.madmeditationapp.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.madmeditationapp.R
import com.example.madmeditationapp.databinding.FragmentDashboardBinding

class DashboardFragment : Fragment() {

private var _binding: FragmentDashboardBinding? = null
private val binding get() = _binding!!

    // This property is only valid between onCreateView and
    // onDestroyView.

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        val root: View = binding.root

       return root

    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
}