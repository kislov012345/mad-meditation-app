package com.example.madmeditationapp.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.madmeditationapp.MyFeel
import com.example.madmeditationapp.MyState
import com.example.madmeditationapp.R
import com.example.madmeditationapp.adapters.FeelAdapter
import com.example.madmeditationapp.adapters.StateAdapter
import com.example.madmeditationapp.databinding.FragmentHomeBinding
import com.example.madmeditationapp.feel

class HomeFragment : Fragment() {


    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        val recyclerView: RecyclerView = root.findViewById(R.id.feel)
        val recyclerView2: RecyclerView = root.findViewById(R.id.state)
        recyclerView2.adapter = StateAdapter(requireContext(), MyState().list2)
        recyclerView.adapter = FeelAdapter(requireContext(), MyFeel().list)


        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}